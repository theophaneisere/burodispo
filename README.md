<h1>BuroDispo</h1>

<p><strong>BuroDispo</strong> est une application web permettant le prêt et la réservation de bureaux dans une <strong>entreprise</strong> ou une <strong>collectivité territoriale.</strong></p>

<h2>Getting Started</h2>

<h3>Informations</h3>

<p>Cette application tourne actuellement sur un serveur avec ces caractéristiques :</p>
<ul>
    <li>Debian 9.1</li>
    <li>MySQL 5.0.12</li>
    <li>Apache 2.4.25</li>
</ul>

<h3>Prérequis</h3>

<p>Serveur en <strong>PHP3</strong> ou plus et la <strong>fonction mail()</strong> activée. (<em>Dans le cas contraire il sera nécessaire de supprimer ou commenter cette fonction dans chacun des fichiers du dossier "backend" qui l'utilise</em>)</p>
<p>Un serveur avec <strong>PhpMyAdmin</strong> installé (Base de données en <strong>SQL</strong>).</p>

<h3>Installation</h3>

<strong>Télécharger</strong> le dossier “BuroDispo”.

<strong>Se connecter</strong> à l’interface <strong>PhpMyAdmin</strong> de votre serveur et <strong>créer</strong> une base de donnée nommée “burodispo” par exemple.

Se rendre dans l’onglet “Importer”, <strong>cliquer</strong> sur “Choisir un fichier”, <strong>sélectionner</strong> le fichier “bdd.sql” qui se trouve dans le dossier “BuroDispo” précédemment téléchargé puis <strong>cliquer</strong> sur “Exécuter”.

<strong>Importer</strong> le dossier “backend” dans le dossier de votre serveur.

Dans le fichier <strong>config.inc.dev.php</strong> insérer les informations de connexion à votre <strong>base de données</strong> (Décommenter et remplir les informations de connexion à l'annuaire LDAP si vous utilisez ce système.)

<strong>Importer tous les autres éléments du dossier “BuroDispo” dans le dossier de votre serveur.

Dans le fichier <strong>main.3914a4ab.js</strong> qui se trouve dans <strong>static/js/</strong>, <strong>remplacer</strong> tous les <strong>" $url_serveur$ "</strong> par l’url de votre serveur (Ne pas mettre de "/" à la fin).

Il est maintenant possible de se connecter à l’aide du compte de test.
<ul>
    <li><strong>username</strong>: Test</li>
    <li><strong>password</strong>: test</li>
</ul>
Il est <strong>indispensable</strong> de créer au moins un lieu dans la partie ADMIN avant que les premiers utilisateurs puissent ajouter leurs bureaux.

Il faut par la suite modifier le fichier <strong>login.php</strong> pour qu’il fasse appelle à l’annuaire de l'entreprise ou de la collectivité et que ce dernier renvoie les informations relatives à chaque agent lorsqu'il se connecte. Si aucun annuaire renvoyant ces informations existe, il faudra créer une page de création de compte ou il sera demandé aux agents de remplir les <Strong>informations nécessaires</Strong> au bon fonctionnement du site (cf. login.php).
Il est possible de décommenter la partie indiquée dans le fichier si vous utilisez un <Strong>annuaire LDAP</Strong>.


