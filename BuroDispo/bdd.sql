-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 25 juil. 2018 à 07:58
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `test`
--

-- --------------------------------------------------------

--
-- Structure de la table `batiments`
--

DROP TABLE IF EXISTS `batiments`;
CREATE TABLE IF NOT EXISTS `batiments` (
  `idBatiment` int(11) NOT NULL AUTO_INCREMENT,
  `idLieu` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`idBatiment`)
) ENGINE=MyISAM AUTO_INCREMENT=6700 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `bureau`;
CREATE TABLE IF NOT EXISTS `bureau` (
  `idBureau` int(11) NOT NULL AUTO_INCREMENT,
  `usernameProprietaire` varchar(10) NOT NULL,
  `bureau` varchar(50) NOT NULL,
  `idBatiment` int(5) NOT NULL,
  `idLieu` int(10) NOT NULL,
  `etage` int(11) NOT NULL,
  `portable` tinyint(1) NOT NULL,
  `fixe` tinyint(1) NOT NULL,
  `phone` tinyint(1) NOT NULL,
  `ethernet` tinyint(1) NOT NULL,
  `reunion` tinyint(1) NOT NULL,
  `partage` int(10) NOT NULL,
  `imprimante` tinyint(1) NOT NULL,
  `caftiere` tinyint(1) NOT NULL,
  `bouilloire` tinyint(1) NOT NULL,
  `refrigerateur` int(11) NOT NULL,
  `microOnde` int(11) NOT NULL,
  `ascenseur` tinyint(1) NOT NULL,
  `image` longblob,
  PRIMARY KEY (`idBureau`)
) ENGINE=MyISAM AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `favoris`;
CREATE TABLE IF NOT EXISTS `favoris` (
  `idFavoris` int(11) NOT NULL AUTO_INCREMENT,
  `idBureau` int(11) NOT NULL,
  `usernameFavoris` varchar(11) NOT NULL,
  PRIMARY KEY (`idFavoris`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `lieu`
--

DROP TABLE IF EXISTS `lieu`;
CREATE TABLE IF NOT EXISTS `lieu` (
  `idLieu` int(10) NOT NULL AUTO_INCREMENT,
  `longitude` double NOT NULL,
  `lattitude` double NOT NULL,
  `nomLieu` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `codePostal` int(5) NOT NULL,
  `nomCommune` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idLieu`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `prets`;
CREATE TABLE IF NOT EXISTS `prets` (
  `idPret` int(11) NOT NULL AUTO_INCREMENT,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `username` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `heureDebut` time NOT NULL,
  `heureFin` time NOT NULL,
  PRIMARY KEY (`idPret`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS `pretsdetails`;
CREATE TABLE IF NOT EXISTS `pretsdetails` (
  `idDetailPret` int(11) NOT NULL AUTO_INCREMENT,
  `idPret` int(11) NOT NULL,
  `dateTranche` date NOT NULL,
  `heureDebutTranche` time NOT NULL,
  `heureFinTranche` time NOT NULL,
  `reserve` tinyint(1) NOT NULL,
  `usernameReservateur` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idDetailPret`)
) ENGINE=MyISAM AUTO_INCREMENT=178 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS `reservations`;
CREATE TABLE IF NOT EXISTS `reservations` (
  `idReservation` int(11) NOT NULL AUTO_INCREMENT,
  `idPret` int(11) NOT NULL,
  `dateReservation` date NOT NULL,
  `heureDebutReservation` time NOT NULL,
  `heureFinReservation` time NOT NULL,
  `usernamereservateur` varchar(10) NOT NULL,
  `notification` int(11) NOT NULL,
  PRIMARY KEY (`idReservation`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `numberPhone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `service` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `receiptMail` tinyint(1) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
