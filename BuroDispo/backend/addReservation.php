<?php 

	header('Content-Type: text/html; charset=utf-8');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $idPret = $obj['idPret'];
    $usernameReservateur = $obj['usernameReservateur'];
    $dateTranche = $obj['dateTranche'];
    $heureDebutTranche = $obj['heureDebutTranche'].':00';
    $heureFinTranche = $obj['heureFinTranche'].':00';
    $completeDate = $obj['completeDate'];
    $i = 0;
    $error = false;


    $test3 =$bdd->query("SELECT * FROM pretsdetails WHERE idPret = '$idPret' and dateTranche = '$dateTranche' and heureDebutTranche >= '$heureDebutTranche' and heureFinTranche <= '$heureFinTranche'");
    while ($donnees = $test3->fetch()) {
        $datesDebut[] = $donnees;
    }
    for($i = 0; $i < $test3->rowCount(); $i++){
        if ($datesDebut[$i]['reserve'] == 1){
            $error = true;
            $i = 10000;
        }
    }
    if ($error){
        $success = false;
    } else {
        $test = $bdd->query("UPDATE pretsdetails SET reserve = 1, usernameReservateur = '$usernameReservateur' WHERE idPret = '$idPret' and dateTranche = '$dateTranche' and heureDebutTranche >= '$heureDebutTranche' and heureFinTranche <= '$heureFinTranche'");
        $test2 = $bdd->query("INSERT INTO reservations (idPret, dateReservation, heureDebutReservation, heureFinReservation, usernameReservateur, notification) VALUES ('$idPret', '$dateTranche', '$heureDebutTranche', '$heureFinTranche','$usernameReservateur', 1)");
        $success = true;
    }



    $mail1 = $bdd->query("SELECT mail, receiptMail from user WHERE user.username = '$usernameReservateur'");
    $mail2 = $bdd->query("SELECT mail, receiptMail, prenom, nom, bureau, batiments.name, nomLieu, etage, nomCommune, lattitude, longitude FROM prets JOIN user ON prets.username = user.username JOIN bureau ON user.username = bureau.usernameProprietaire JOIN batiments ON bureau.idBatiment = batiments.idBatiment JOIN lieu ON batiments.idLieu = lieu.idLieu WHERE prets.idPret = '$idPret'");

    if ($mail1->rowCount() > 0){
        while ($donnees1 = $mail1->fetch()) {
            $resulset1[] = $donnees1;
        }
    } else {
        $resulset1 = null;
    }
    if ($mail2->rowCount() > 0){
        while ($donnees2 = $mail2->fetch()) {
            $resulset2[] = $donnees2;
        }
    } else {
        $resulset2 = null;
    }

    
        $header1="MIME-Version: 1.0\r\n";
        $header1.='From: <BuroDispo>'."\n";
        $header1.='Content-Type:text/html; charset="uft-8"'."\n";
        $header1.='Content-Transfer-Encoding: 8bit';
        $message1="
        <html>
            <body style='font-family: Arial; padding: 0;margin: 0;'>
                <h3 style='background-color: #0060aa;color: #fff;padding: 10px;text-align: center;'>F�licitations, votre r�servation a bien �t� enregistr�e !</h3>
            	<p style='margin: 0';>".$completeDate."</p>
		<table style='width: 100%;border-top: 1px solid #ccc;font-family: Arial'>
			<tr>
				<td>".$obj['heureDebutTranche']."h00<br/>".$obj['heureFinTranche']."h00</td>
				<td><a href='https://www.google.fr/maps?f=q&hl=fr&q=".$resulset2[0]['lattitude'].",".$resulset2[0]['longitude']."' target='_blank'>".utf8_decode($resulset2[0]['nomLieu'])."</a></td>
				<td>B�timent/Aile ".utf8_decode($resulset2[0]['name'])."</td>
				<td>Etage ".$resulset2[0]['etage']."</td>
				<td>Bureau ".utf8_decode($resulset2[0]['bureau'])."</td>
			</tr>
		</table>
            	<p>A bient�t sur BuroDispo !</p>
            </body>
        </html>
        ";

        $header2="MIME-Version: 1.0\r\n";
        $header2.='From: <BuroDispo>'."\n";
        $header2.='Content-Type:text/html; charset="uft-8"'."\n";
        $header2.='Content-Transfer-Encoding: 8bit';
        $message2="
        <html>
            <body style='font-family: Arial'>
                <h3 style='background-color: #0060aa;color: #fff;padding: 10px;text-align: center;'>Un agent a r�serv� votre bureau !</h3>
                <p>Un agent a r�serv� votre bureau le ".$completeDate." de ".$heureDebutTranche." � ".$heureFinTranche." !</p>
                <p>Si vous devez annuler votre pr�t, nous vous invitons à le faire le plus rapidement possible afin que la ou les personne(s) l'ayant r�serv� soient pr�venues et puisse(nt) s'adapter.</p>
                <p>A bient�t sur BuroDispo !</p>
            </body>
        </html>
        ";

	if ($resulset1[0]['receiptMail'] == 0){
        	@mail($resulset1[0]['mail'], "Reservation effectuee avec succes", $message1, $header1);
	}
	if ($resulset2[0]['receiptMail'] == 0){
        	@mail($resulset2[0]['mail'], "Quelqu'un a reserve votre bureau", $message2, $header2);
	}
    
    
    echo json_encode(array(
        'success' => $success,
    ));
    
?>