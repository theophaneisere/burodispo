<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $username = $obj['username'];
    $currentDate = $obj['currentDate'];

    $reponse = $bdd->query("SELECT * FROM reservations JOIN prets ON reservations.idPret = prets.idPret JOIN user ON user.username = prets.username JOIN bureau ON bureau.usernameProprietaire = user.username JOIN lieu ON lieu.idLieu = bureau.idLieu JOIN batiments ON bureau.idBatiment = batiments.idBatiment WHERE reservations.usernamereservateur = '$username' and reservations.dateReservation >= '$currentDate' ORDER BY dateReservation, heureDebutReservation"); //Recupere les reservations prises l'utilisateur

    if ($reponse->rowCount() > 0){
        while ($donnees = $reponse->fetch()) {
            $resultset[] = $donnees;
        }
    } else {
        $resultset[] = null;
    }

    echo json_encode(array(
        'infosReservations' => $resultset,
        'nbReservations' => $reponse->rowCount(),
    ));