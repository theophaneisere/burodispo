<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $username = $obj['username'];
    $dateDebut = $obj['dateDebut'];
    $dateFin = $obj['dateFin'];
    $nbDateDebut = $obj['nbDateDebut'];
    $nbDateFin = $obj['nbDateFin'];
    $heureDebut = $obj['heureDebut'];
    $heureFin = $obj['heureFin'];
    $nbHeureDebut = $obj['nbHeureDebut'];
    $nbHeureFin = $obj['nbHeureFin'];
    $message = 'riz';
    $nbJours = 0;
    $i = 0;
    $nbResults = 0;

    $test = $bdd->query("SELECT idPret FROM prets 
    	WHERE (
    		prets.username = '$username' 
    		AND (prets.dateDebut > '$dateDebut' OR (prets.dateDebut = '$dateDebut' AND prets.heureDebut >= '$heureDebut')) 
    		AND (prets.dateDebut < '$dateFin' OR (prets.dateDebut = '$dateFin' AND prets.heureDebut < '$heureFin'))
    	) OR (
    		prets.username = '$username' 
    		AND (prets.dateDebut < '$dateDebut' OR (prets.dateDebut = '$dateDebut' AND prets.heureDebut < '$heureDebut'))
    		AND (prets.dateFin > '$dateDebut' OR (prets.dateFin = '$dateDebut' AND prets.heureFin > '$heureDebut'))
    	) OR (
    		prets.username = '$username' 
    		AND (prets.dateDebut < '$dateFin' OR (prets.dateDebut = '$dateFin' AND prets.heureDebut < '$heureFin'))
    		AND (prets.dateFin > '$dateFin' OR (prets.dateFin = '$dateFin' AND prets.heureFin > '$heureFin'))
    	)
    	");

    if ($test->rowCount() == 0){
    	$resultset = 'rien';
    } else {
    	while ($donnees = $test->fetch()) {
	        $resultset[] = $donnees;
	    };
    }
    $nbResults = $test->rowCount();
    
    for ($i = 0; $i < $nbResults; $i++){
    	$prout = $resultset[$i]['idPret'];
        $test2 = $bdd->query("DELETE FROM pretsdetails WHERE idPret = '$prout'");
    }


	$test3 = $bdd->query("DELETE FROM prets 
    	WHERE (
    		prets.username = '$username' 
    		AND (prets.dateDebut > '$dateDebut' OR (prets.dateDebut = '$dateDebut' AND prets.heureDebut >= '$heureDebut')) 
    		AND (prets.dateDebut < '$dateFin' OR (prets.dateDebut = '$dateFin' AND prets.heureDebut < '$heureFin'))
    	) OR (
    		prets.username = '$username' 
    		AND (prets.dateDebut < '$dateDebut' OR (prets.dateDebut = '$dateDebut' AND prets.heureDebut < '$heureDebut'))
    		AND (prets.dateFin > '$dateDebut' OR (prets.dateFin = '$dateDebut' AND prets.heureFin > '$heureDebut'))
    	) OR (
    		prets.username = '$username' 
    		AND (prets.dateDebut < '$dateFin' OR (prets.dateDebut = '$dateFin' AND prets.heureDebut < '$heureFin'))
    		AND (prets.dateFin > '$dateFin' OR (prets.dateFin = '$dateFin' AND prets.heureFin > '$heureFin'))
    	)
    	");
	$reponse = $bdd->query("INSERT INTO prets (dateDebut, dateFin, username, heureDebut, heureFin) VALUES ('$dateDebut', '$dateFin', '$username', '$heureDebut', '$heureFin')");
        $reponse2 = $bdd->query("SELECT LAST_INSERT_ID() as idPret FROM prets");

        while ($donnees = $reponse2->fetch()) {
            $idPret = $donnees['idPret'];
        }
        for ($i = $nbDateDebut; $i <= $nbDateFin; $i++){
            $nbJours++;
        }
        if ($nbJours == 1){
            for ($i = $nbHeureDebut; $i < $nbHeureFin; $i++) {
                $heureDebutTranche = $i.':00';
                $heureFinTrancheF = $i+1;
                $heureFinTranche = $heureFinTrancheF.':00';
                $reponse = $bdd->query("INSERT INTO pretsdetails (idPret, dateTranche, heureDebutTranche, heureFinTranche, reserve, usernameReservateur) VALUES ('$idPret', '$dateDebut', '$heureDebutTranche', '$heureFinTranche', false, 0)");
            }
        } else if ($nbJours >= 2){
            for ($i = $nbDateDebut; $i <= $nbDateFin; $i++){
                if (substr($i, -2, 2) < 32){
                    if ($i == $nbDateDebut){
                        for ($j = $nbHeureDebut; $j < $nbHeureFin; $j++) {
                            $heureDebutTranche = $j.':00';
                            $heureFinTrancheF = $j+1;
                            $heureFinTranche = $heureFinTrancheF.':00';
                            $reponse = $bdd->query("INSERT INTO pretsdetails (idPret, dateTranche, heureDebutTranche, heureFinTranche, reserve, usernameReservateur) VALUES ('$idPret', '$dateDebut', '$heureDebutTranche', '$heureFinTranche', false, 0)");
                        }
                    } else if ($i == $nbDateFin){
                        for ($j = 8; $j < $nbHeureFin; $j++) {
                            $heureDebutTranche = $j.':00';
                            $heureFinTrancheF = $j+1;
                            $heureFinTranche = $heureFinTrancheF.':00';
                            $reponse = $bdd->query("INSERT INTO pretsdetails (idPret, dateTranche, heureDebutTranche, heureFinTranche, reserve, usernameReservateur) VALUES ('$idPret', '$dateFin', '$heureDebutTranche', '$heureFinTranche', false, 0)");
                        }
                    } else {
                        for ($j = 8; $j < 18; $j++) {
                            $date = substr($i, -8, 4).'-'.substr($i, -4, 2).'-'.substr($i, -2, 2);
                            $heureDebutTranche = $j.':00';
                            $heureFinTrancheF = $j+1;
                            $heureFinTranche = $heureFinTrancheF.':00';
                            $reponse = $bdd->query("INSERT INTO pretsdetails (idPret, dateTranche, heureDebutTranche, heureFinTranche, reserve,usernameReservateur) VALUES ('$idPret', '$date', '$heureDebutTranche', '$heureFinTranche', false, 0)");
                        }
                    }
                }
            }
        }



    if($test){
		echo json_encode(array(
	        'success' => true,
	    ));
	} else {
		echo json_encode(array(
	        'success' => false,
	    ));
	}
    
?>