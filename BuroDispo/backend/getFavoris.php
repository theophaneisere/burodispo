<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $username = $obj['username'];

    $reponse = $bdd->query("SELECT * FROM favoris JOIN bureau ON favoris.idBureau = bureau.idBureau JOIN user ON bureau.usernameProprietaire = user.username JOIN lieu ON bureau.idLieu = lieu.idLieu JOIN batiments ON bureau.idBatiment = batiments.idBatiment WHERE favoris.usernameFavoris = '$username'");

    if ($reponse){
        if ($reponse->rowCount() > 0){
            while ($donnees = $reponse->fetch()) {
                $resultset[] = $donnees;
            }
            $nbFavoris = $reponse->rowCount();
        } else {
            $resultset = null;
            $nbFavoris = 0;
        }
    }

    echo json_encode(array(
        'infosFavoris' => $resultset,
        'nbFavoris' => $nbFavoris,
    ));
?>