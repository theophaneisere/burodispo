<html>
	<head>
		<link rel="stylesheet" href="./styleAdmin.css">
	</head>
	<body>
		<div class="header">
          <ul class="menu">
            <li>
                <h1>Bureau mobile ADMIN</h1>
            </li>
            <li>
              <a href="#">Tableau de bord</a>
            </li>
            <li>
              <a href="#">Utilisateurs</a>
            </li>
            <li>
             <a href="#">Trafic</a>
            </li>
          </ul>
        </div>
        <div class="container">
			<h1>Liste des utilisateurs</h1>
			<table>
				<tr>
					<td>id</td>
	    			<td>Prénom</td>
	    			<td>Nom</td>
	    			<td>Service</td>
	    			<td>Email</td>
	    			<td>Téléphone</td>
	    			<td></td>
				</tr>
			<?php 
			try {
				$bdd = new PDO('mysql:host='.$db_host.';dbname='.$db_name.';charset=utf8', $db_username, $db_password);
			}
			catch (Exception $e){
				die('Erreur : ' . $e->getMessage());
			};
			$reponse = $bdd->query("SELECT * FROM user");
			while ($donnees = $reponse->fetch()) {
		        $resultset[] = $donnees;
		    };
		    foreach ($resultset as $key) {
		    	echo '
		    		<tr>
		    			<td>'.$key['id'].'</td>
		    			<td>'.$key['prenom'].'</td>
		    			<td>'.$key['nom'].'</td>
		    			<td>'.$key['service'].'</td>
		    			<td>'.$key['mail'].'</td>
		    			<td>'.$key['numberPhone'].'</td>
		    			<td><a href="#">Voir activité</td>
		    		</tr>';
		    }
			?>
			</table>
		</div>

	</body>