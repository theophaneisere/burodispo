<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $idReservation = $obj['idReservation'];
    $idPret = $obj['idPret'];
    $heureDebutReservation = $obj['heureDebutReservation'];
    $heureFinReservation = $obj['heureFinReservation'];

    $test2 = $bdd->query("UPDATE pretsdetails SET reserve = 0, usernameReservateur = 0 WHERE idPret = '$idPret' and heureDebutTranche >= '$heureDebutReservation' and heureFinTranche <= '$heureFinReservation'");
    $test3 = $bdd->query("DELETE FROM reservations WHERE idReservation = '$idReservation'");
    if ($test2 and $test3) {

	    echo json_encode(array(
	        'success' => true
	    ));
	}
?>