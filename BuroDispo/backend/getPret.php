<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $id = $obj['id'];
    $username = $obj['username'];
    $date = $obj['date'];
    $hourD = $obj['hourD'];
    $hourF = $obj['hourF'];

    $reponse = $bdd->query("SELECT * FROM pretsdetails JOIN prets ON pretsdetails.idPret = prets.idPret JOIN user ON user.username = prets.username JOIN bureau ON bureau.usernameProprietaire = user.username JOIN lieu ON lieu.idLieu = bureau.idLieu JOIN batiments ON bureau.idBatiment = batiments.idBatiment WHERE pretsdetails.idPret = '$id' AND dateTranche = '$date' and heureDebutTranche >= '$hourD' and heureFinTranche <= '$hourF' ORDER BY pretsdetails.idDetailPret");
    $reponse2 = $bdd->query("SELECT * FROM favoris WHERE favoris.usernameFavoris = '$username'");
    if ($reponse->rowCount() == 0){
    	$resultset = 'rien';
    } else {
    	while ($donnees = $reponse->fetch()) {
	        $resultset[] = $donnees;
	    };
    }
    if ($reponse2->rowCount() == 0){
        $resultsetFavoris = 'rien';
        $nbFavoris = 0;
    } else {
        while ($donnees = $reponse2->fetch()) {
            $resultsetFavoris[] = $donnees;
        };
        $nbFavoris = $reponse2->rowCount();
    }
    echo json_encode(array(
        'infos' => $resultset,
        'nbTranches' => $reponse->rowCount(),
        'infosFavoris' => $resultsetFavoris,
        'nbFavoris' => $nbFavoris
    ));
?>