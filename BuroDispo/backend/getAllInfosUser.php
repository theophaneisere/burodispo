<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
	try {
		
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
	}
	catch (Exception $e){
	    die('Erreur : ' . $e->getMessage());
	};
	$json = file_get_contents('php://input'); 	
	$obj = json_decode($json,true);
	$username = $obj['username'];

	$reponse = $bdd->query("SELECT * FROM user WHERE user.username = '$username'");
	while ($donnees = $reponse->fetch()) {
			$resulset[] = $donnees;
		}

	$reponse1 = $bdd->query("SELECT * FROM prets JOIN user ON user.username = prets.username JOIN bureau ON bureau.usernameProprietaire = user.username JOIN lieu ON lieu.idLieu = bureau.idLieu JOIN batiments ON bureau.idBatiment = batiments.idBatiment WHERE user.username = '$username' ORDER BY dateDebut, heureDebut");
	if ($reponse1->rowCount() > 0){
		while ($donnees = $reponse1->fetch()) {
			$resulset1[] = $donnees;
		}
	} else {
		$resulset1 = null;
	}
	$reponse2 =$bdd->query("SELECT * FROM reservations JOIN prets ON reservations.idPret = prets.idPret JOIN user ON user.username = prets.username JOIN bureau ON bureau.usernameProprietaire = user.username JOIN lieu ON lieu.idLieu = bureau.idLieu JOIN batiments ON bureau.idBatiment = batiments.idBatiment WHERE reservations.usernamereservateur = '$username' ORDER BY dateReservation, heureDebutReservation");
	if ($reponse2->rowCount() > 0){
		while ($donnees = $reponse2->fetch()) {
			$resulset2[] = $donnees;
		}
	} else {
		$resulset2 = null;
	}
	echo json_encode(array(
		'infosUser' => $resulset,
		'infosPrets' => $resulset1,
		'infosReservations' => $resulset2,
	));
?>