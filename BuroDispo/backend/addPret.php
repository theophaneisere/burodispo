<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $username = $obj['username'];
    $dateDebut = $obj['dateDebut'];
    $dateFin = $obj['dateFin'];
    $nbDateDebut = $obj['nbDateDebut'];
    $nbDateFin = $obj['nbDateFin'];
    $heureDebut = $obj['heureDebut'];
    $heureFin = $obj['heureFin'];
    $nbHeureDebut = $obj['nbHeureDebut'];
    $nbHeureFin = $obj['nbHeureFin'];
    $completeDateDebut = $obj['completeDateDebbut'];
    $completeDateFin = $obj['completeDateFin'];
    $message = 'riz';
    $nbJours = 0;
    if ($nbHeureDebut == 8 or $nbHeureDebut == 9){
        $nbDateHeureDebut = intval($nbDateDebut.'0'.$nbHeureDebut);
    } else {
        $nbDateHeureDebut = intval($nbDateDebut.$nbHeureDebut);
    }
    if ($nbHeureFin == 9){
        $nbDateHeureFin = intval($nbDateFin.'0'.$nbHeureFin);
    } else {
        $nbDateHeureFin = intval($nbDateFin.$nbHeureFin);
    }


    $datas = false;
    $test = $bdd->query("SELECT mail, receiptMail, prets.username, dateDebut, heureDebut, dateFin, heureFin FROM prets JOIN user ON user.username = prets.username WHERE prets.username = '$username'");
    while ($donnees = $test->fetch()) {
        $datesDebut[] = $donnees;
        $datas = true;
    }
    $error = false;
    if ($datas == true){
        foreach ($datesDebut as $value) {
            $testDateHeureDebut = intval(substr($value['dateDebut'], -10, 4).substr($value['dateDebut'], -5, 2).substr($value['dateDebut'], -2, 2).substr($value['heureDebut'], -8, 2));
            $testDateHeureFin = intval(substr($value['dateFin'], -10, 4).substr($value['dateFin'], -5, 2).substr($value['dateFin'], -2, 2).substr($value['heureFin'], -8, 2));
            if (($nbDateHeureDebut < $testDateHeureDebut and $nbDateHeureFin <= $testDateHeureDebut) or ($nbDateHeureDebut >= $testDateHeureFin and $nbDateHeureFin > $testDateHeureFin)){
            } else {
                $error = true;
                $success = false;
            }
        }
    }
    if ($error == false){
        $reponse = $bdd->query("INSERT INTO prets (dateDebut, dateFin, username, heureDebut, heureFin) VALUES ('$dateDebut', '$dateFin', '$username', '$heureDebut', '$heureFin')");
        $reponse2 = $bdd->query("SELECT LAST_INSERT_ID() as idPret FROM prets");

        while ($donnees = $reponse2->fetch()) {
            $idPret = $donnees['idPret'];
        }
        for ($i = $nbDateDebut; $i <= $nbDateFin; $i++){
            $nbJours++;
        }
        if ($nbJours == 1){
            for ($i = $nbHeureDebut; $i < $nbHeureFin; $i++) {
                $heureDebutTranche = $i.':00';
                $heureFinTrancheF = $i+1;
                $heureFinTranche = $heureFinTrancheF.':00';
                $reponse = $bdd->query("INSERT INTO pretsdetails (idPret, dateTranche, heureDebutTranche, heureFinTranche, reserve, usernameReservateur) VALUES ('$idPret', '$dateDebut', '$heureDebutTranche', '$heureFinTranche', false, 0)");
            }
        } else if ($nbJours >= 2){
            for ($i = $nbDateDebut; $i <= $nbDateFin; $i++){
                if (substr($i, -2, 2) < 32){
                    if ($i == $nbDateDebut){
                        for ($j = $nbHeureDebut; $j < $nbHeureFin; $j++) {
                            $heureDebutTranche = $j.':00';
                            $heureFinTrancheF = $j+1;
                            $heureFinTranche = $heureFinTrancheF.':00';
                            $reponse = $bdd->query("INSERT INTO pretsdetails (idPret, dateTranche, heureDebutTranche, heureFinTranche, reserve, usernameReservateur) VALUES ('$idPret', '$dateDebut', '$heureDebutTranche', '$heureFinTranche', false, 0)");
                        }
                    } else if ($i == $nbDateFin){
                        for ($j = 8; $j < $nbHeureFin; $j++) {
                            $heureDebutTranche = $j.':00';
                            $heureFinTrancheF = $j+1;
                            $heureFinTranche = $heureFinTrancheF.':00';
                            $reponse = $bdd->query("INSERT INTO pretsdetails (idPret, dateTranche, heureDebutTranche, heureFinTranche, reserve, usernameReservateur) VALUES ('$idPret', '$dateFin', '$heureDebutTranche', '$heureFinTranche', false, 0)");
                        }
                    } else {
                        for ($j = 8; $j < 18; $j++) {
                            $date = substr($i, -8, 4).'-'.substr($i, -4, 2).'-'.substr($i, -2, 2);
                            $heureDebutTranche = $j.':00';
                            $heureFinTrancheF = $j+1;
                            $heureFinTranche = $heureFinTrancheF.':00';
                            $reponse = $bdd->query("INSERT INTO pretsdetails (idPret, dateTranche, heureDebutTranche, heureFinTranche, reserve,usernameReservateur) VALUES ('$idPret', '$date', '$heureDebutTranche', '$heureFinTranche', false, 0)");
                        }
                    }
                }
            }
        }
        $success = true;
    }

    $mail = $bdd->query("SELECT mail, receiptMail FROM user WHERE username = '$username'"); 
    if ($mail->rowCount() > 0){
        while ($donnees = $mail->fetch()) {
            $resulset[] = $donnees;
        }
    } else {
        $resulset = null;
    }

    if (!$error and $success){
	if ($resulset[0]['receiptMail'] == 0){
        	$header="MIME-Version: 1.0\r\n";
        	$header.='From: <BuroDispo>'."\n";
        	$header.='Content-Type:text/html; charset="uft-8"'."\n";
        	$header.='Content-Transfer-Encoding: 8bit';

        	$message="
        	<html>
            		<body style='font-family: Arial; padding: 0;margin: 0;'>
                		<h3 style='background-color: #0060aa;color: #fff;padding: 10px;text-align: center;'>F�licitations, votre proposition de pr�t a bien �t� enregistr�e !</h3>
                		<p>Votre pr�t du ".$completeDateDebut." � ".$heureDebut." au ".$completeDateFin." � ".$heureFin." a bien �t� mis en ligne. Vous serez inform�(e) d�s que quelqu'un effectuera une r�servation.</p>
                		<p>A bient�t sur BuroDispo !</p>
            		</body>
        	</html>
        	";

        	@mail($resulset[0]['mail'], "Proposition de pret enregistree", $message, $header);
	}
    }

    echo json_encode(array(
        'message' => $error,
        'success' =>$success
    ));
    
?>