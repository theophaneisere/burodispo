<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
	try {
        
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $username = $obj['username'];
    $dateDebut = $obj['dateDebutRecurrence'];
    $day = $obj['day'];
    $nbWeeks = $obj['duree'];
    $heureDebut = $obj['heureDebutRecurrence'];
    $heureFin = $obj['heureFinRecurrence'];
    $nbHeureDebut = intval(substr($heureDebut, -5, 2));
    $nbHeureFin = intval(substr($heureFin, -5, 2));
	$i = $dateDebut;
    $datas = false;
    if ($day === 'Mon'){
	$jour = 'lundi';
    } else if($day === 'Tue'){
	$jour = 'mardi';
    } else if($day === 'Wed'){
	$jour = 'mercredi';
    } else if($day === 'Thu'){
	$jour = 'jeudi';
    } else if($day === 'Fri'){
	$jour = 'vendredi';
    }
    $test = $bdd->query("SELECT user.mail, user.receiptMail, prets.username, dateDebut, heureDebut, dateFin, heureFin FROM prets JOIN user ON user.username = prets.username WHERE prets.username = '$username'");
    while ($donnees = $test->fetch()) {
        $datesDebut[] = $donnees;
        $datas = true;
    }
    if ($datas == false){
    	$datesDebut[] = '';
    }

	for ($j = 0; $j < $nbWeeks;){
		if (substr($i, -2, 2) == 32 AND (substr($i, -4, 2) == 1 or substr($i, -4, 2) == 3 or substr($i, -4, 2) == 5 or substr($i, -4, 2) == 7 or substr($i, -4, 2) == 8)){
			$i = substr($i, -8, 4).'0'.(substr($i, -4, 2)+1).'01';
		} else if (substr($i, -2, 2) == 32 AND substr($i, -4, 2) == 10){
			$i = substr($i, -8, 4).(substr($i, -4, 2)+1).'01';
		} else if (substr($i, -2, 2) == 31 AND (substr($i, -4, 2) == 4 or substr($i, -4, 2) == 6)){
			$i = substr($i, -8, 4).'0'.(substr($i, -4, 2)+1).'01';
		} else if (substr($i, -2, 2) == 31 AND (substr($i, -4, 2) == 9 or substr($i, -4, 2) == 11)){
			$i = substr($i, -8, 4).(substr($i, -4, 2)+1).'01';
		} else if (substr($i, -2, 2) == 32 AND substr($i, -4, 2) == 12){
			$i = (substr($i, -8, 4)+1).'0101';
		} else if (substr($i, -4, 2) == 02){
			if (ctype_digit(substr($i, -8, 4)/4) AND substr($i, -2, 2) == 30){
				$i = substr($i, -8, 4).'0'.(substr($i, -4, 2)+1).'01';
			} else if (!ctype_digit(substr($i, -8, 4)/4) AND substr($i, -2, 2) == 29){
				$i = substr($i, -8, 4).'0'.(substr($i, -4, 2)+1).'01';
			}
		}
		$timestamp = mktime(0, 0, 0, substr($i, -4, 2), substr($i, -2, 2), substr($i, -8, 4));
		if (date('D', $timestamp) == $day){
			$j++;
			$date = substr($i, -8, 4).'-'.substr($i, -4, 2).'-'.substr($i, -2, 2);
			if ($nbHeureDebut == 8 or $nbHeureDebut == 9){
		        $nbDateHeureDebut = intval($i.'0'.$nbHeureDebut);
		    } else {
		        $nbDateHeureDebut = intval($i.$nbHeureDebut);
		    }
		    if ($nbHeureFin == 9){
		        $nbDateHeureFin = intval($i.'0'.$nbHeureFin);
		    } else {
		        $nbDateHeureFin = intval($i.$nbHeureFin);
		    }
		    $error = false;
		    if ($datas == true){
		        foreach ($datesDebut as $value) {
		            $testDateHeureDebut = intval(substr($value['dateDebut'], -10, 4).substr($value['dateDebut'], -5, 2).substr($value['dateDebut'], -2, 2).substr($value['heureDebut'], -8, 2));
		            $testDateHeureFin = intval(substr($value['dateFin'], -10, 4).substr($value['dateFin'], -5, 2).substr($value['dateFin'], -2, 2).substr($value['heureFin'], -8, 2));
		            if (($nbDateHeureDebut < $testDateHeureDebut and $nbDateHeureFin <= $testDateHeureDebut) or ($nbDateHeureDebut >= $testDateHeureFin and $nbDateHeureFin > $testDateHeureFin)){
		            } else {
		                $error = true;
		                $success = false;
		            }
		        }
		    } else {
		    	$testDateHeureDebut = '';
		    }
		    if (!$error){
		        $reponse = $bdd->query("INSERT INTO prets (dateDebut, dateFin, username, heureDebut, heureFin) VALUES ('$date', '$date', '$username', '$heureDebut', '$heureFin')");
		        $reponse2 = $bdd->query("SELECT LAST_INSERT_ID() as idPret FROM prets");
		        while ($donnees = $reponse2->fetch()) {
		            $idPret = $donnees['idPret'];
		        }
		        for ($k = $nbHeureDebut; $k < $nbHeureFin; $k++) {
	                $heureDebutTranche = $k.':00';
	                $heureFinTrancheF = $k+1;
	                $heureFinTranche = $heureFinTrancheF.':00';
	                $reponse = $bdd->query("INSERT INTO pretsdetails (idPret, dateTranche, heureDebutTranche, heureFinTranche, reserve, usernameReservateur) VALUES ('$idPret', '$date', '$heureDebutTranche', '$heureFinTranche', false, 0)");
	     

		    }
	        }
	    }
		$i++;
	}
	if ($datesDebut[0]['receiptMail'] === 0){

			$header="MIME-Version: 1.0\r\n";
        		$header.='From: <BuroDispo>'."\n";
        		$header.='Content-Type:text/html; charset="uft-8"'."\n";
        		$header.='Content-Transfer-Encoding: 8bit';

        		$message="
        		<html>
            		    <body style='font-family: Arial; padding: 0;margin: 0;'>
                		<h3 style='background-color: #0060aa;color: #fff;padding: 10px;text-align: center;'>F�licitations, votre proposition de pr�t hebdomadaire a bien �t� enregistr�e !</h3>
                		<p>Votre pr�t hebdomadaire du $jour de ".$heureDebut." � ".$heureFin." a bien �t� enregistr� pour une dur�e de ".$nbWeeks." semaines. Vous serez inform�(e) d�s que quelqu'un effectuera une r�servation.</p>
                		<p>A bient�t sur BuroDispo !</p>
            		    </body>
        		</html>
        		";

        		@mail($datesDebut[0]['mail'], "Proposition de pret hebdomadaire enregistree", $message, $header);

	


	}
	echo json_encode(array(
        'success' => true,
    ));
	?>
