<?php 
	header('Access-Control-Allow-Origin: *'); 
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS'); 
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
        include 'config.inc.dev.php';
    
        $json = file_get_contents('php://input');   
        $obj = json_decode($json,true);
        $username = $obj['username'];
        $password = $obj['password'];
        $alreadyLog = false;
        $admin = 0;

        if ($password == 'test' and $username == 'Test'){ //Vérification identifiant et mot de passe


		//Informations à récuperer
            $success = true;
            $nom = 'Test';
            $prenom = 'Test';
            $numberPhone = 'Test';
            $mail = 'test@test.fr';
            $service = 'TEST';
            $admin = '1';
		//---------------------------//
		

		// SI ANNUAIRE LDAP supprimer lignes 13 à 24 et décommenter celles de dessous en entrant les informations de connexion nécessaires
			/*
			$ldaphost ="$aRemplir$";
			$ldapport ="$aRemplir$";
			$dn=("OU=utilisateurs,DC=$aRemplir$,DC=$aRemplir$");


			if($cn and $passwd){
				
				//connexion à  l'annuaire
				$connect=ldap_connect($ldaphost) or die("Une erreur est intervenue:impossible de se connecter au serveur LDAP/NDS");
				ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
				ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
				//Tous les utilisateurs de l'AD ayant le droit de lecture sur l'annuaire on exploite cela pour vérifier que les données saisies sont
				//bonnes sinon la fonction ldap_bind() renvoie false.
				$identification = @ldap_bind($connect,$cn."@".$ldaphost,$passwd);
				if($identification) //si on est connecté à  l'AD c'est que l'identifiant et le mot de passe sont bon, on peut donc récupérer des infos
				{
					$success=true;
					$sr=ldap_search($connect, $dn, "cn=$cn");
					$info=ldap_get_entries($connect, $sr);
					$count_extract_dn=$info["count"]; //compte le nombre d'enregistrements retournés
					if ($count_extract_dn !=0) //résultat trouvé
					{
							$cn1=$info[0]["cn"][0];
							$mail=$info[0]["mail"][0];
							$nom=$info[0]["sn"][0];
							$prenom=$info[0]["givenname"][0];
							$service=$info[0]["department"][0];
							$numberPhone=$info[0]["telephonenumber"][0];
					}
				}
			*/

            try {
                $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
            }

            catch (Exception $e){
                die('Erreur : ' . $e->getMessage());
            };

            $reponse = $bdd->query("SELECT username, admin FROM user");
            while ($donnees = $reponse->fetch()) {
                if($donnees['username'] == $username){
                    $alreadyLog = true;
                    $admin = $donnees['admin'];
                }
            }

            if ($alreadyLog){
                $update = $bdd->query("UPDATE user SET nom = '$nom', prenom = '$prenom', numberPhone = '$numberPhone', mail = '$mail', service = '$service' WHERE username = '$username'");
            } else {
                $insert = $bdd->query("INSERT INTO user (username, nom, prenom, numberPhone, mail, service, receiptMail, admin) VALUES ('$username', '$nom', '$prenom', '$numberPhone', '$mail', '$service', '0', '0')");
            }


        
            echo json_encode(array(
                'success' => $success,
                'username' => $username,
                'nom' => $nom,
                'prenom' => $prenom,
                'numberPhone' => $numberPhone,
                'mail' => $mail,
                'service' => $service,
                'admin' => $admin,
            ));

        } else {
            echo json_encode(array(
                'success' => false,
                'username' => '',
                'nom' => '',
                'prenom' => '',
                'numberPhone' => '',
                'mail' => '',
                'service' => '',
                'admin' => '0',
            ));
        }
?>
