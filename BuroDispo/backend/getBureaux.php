<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $username = $obj['username'];
    $search = $obj['search'];
    $day = $obj['day'];
    $hour = $obj['hour'];

    $reponse = $bdd->query("SELECT * FROM pretsdetails JOIN prets ON pretsdetails.idPret = prets.idPret JOIN user ON user.username = prets.username JOIN bureau ON bureau.usernameProprietaire = user.username JOIN lieu ON lieu.idLieu = bureau.idLieu JOIN batiments ON bureau.idBatiment = batiments.idBatiment WHERE lieu.nomLieu = '$search' AND dateTranche = '$day' and heureDebutTranche >= '$hour' ORDER BY prets.heureDebut, idDetailPret");
    $reponse2 = $bdd->query("SELECT * FROM favoris WHERE favoris.usernameFavoris = '$username'");
    if ($reponse->rowCount() == 0){
    	$resultset = 'rien';
    } else {
    	while ($donnees = $reponse->fetch()) {
	        $resultset[] = $donnees;
	    };
    }
    if ($reponse2->rowCount() == 0){
        $resultsetFavoris = 'rien';
        $nbFavoris = 0;
    } else {
        while ($donnees = $reponse2->fetch()) {
            $resultsetFavoris[] = $donnees;
        };
        $nbFavoris = $reponse2->rowCount();
    }
    echo json_encode(array(
        'infos' => $resultset,
        'nbBureaux' => $reponse->rowCount(),
        'infosFavoris' => $resultsetFavoris,
        'nbFavoris' => $nbFavoris
    ));
?>