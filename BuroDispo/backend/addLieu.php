<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $nomLieu = $obj['nomLieu'];
    $longLieu = $obj['longLieu'];
    $latLieu = $obj['latLieu'];
    $codePostal = $obj['codePostal'];
    $commune = $obj['commune'];
    $batiments = $obj['batiments'];

    $reponse = $bdd->query("INSERT INTO lieu (longitude, lattitude, nomLieu, codePostal, nomCommune) VALUES ('$longLieu', '$latLieu', '$nomLieu', '$codePostal', '$commune')");
    $reponse1 = $bdd->query("SELECT idLieu FROM lieu WHERE nomLieu = '$nomLieu'");
    $reponse2 = $bdd->query("SELECT LAST_INSERT_ID() as idLieu FROM lieu");
    while ($donnees = $reponse1->fetch()) {
        $idLieu = $donnees['idLieu'];
    };
    for ($i = 0; $i < 20; $i++){
        if ($batiments[$i] !== ''){
            $reponse2 = $bdd->query("INSERT INTO batiments (idLieu, name) VALUES ('$idLieu', '$batiments[$i]')");
        }
    }
    if ( $reponse) {
	    echo json_encode(array(
	        'success' => true
	    ));
	}
?>