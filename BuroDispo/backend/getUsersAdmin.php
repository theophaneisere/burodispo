<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
	try {
		$bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
	}
	catch (Exception $e){
		die('Erreur : ' . $e->getMessage());
	};

	$reponse = $bdd->query("SELECT * FROM user LEFT JOIN bureau ON bureau.usernameProprietaire = user.username LEFT JOIN lieu ON lieu.idLieu = bureau.idLieu ORDER BY id");
	while ($donnees = $reponse->fetch()) {
        $resultset[] = $donnees;
    };
   	echo json_encode(array(
        'users' => $resultset,
        'nbUsers' => $reponse->rowCount(),
    ));
?>