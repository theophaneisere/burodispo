<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $idLieu = $obj['idLieu'];

    $reponse = $bdd->query("DELETE lieu, bureau, prets, pretsdetails, reservations, batiments FROM lieu LEFT JOIN bureau on bureau.idLieu = lieu.idLieu LEFT JOIN prets on prets.username = bureau.usernameProprietaire LEFT JOIN pretsdetails on pretsdetails.idPret = prets.idPret LEFT JOIN reservations on reservations.idPret = prets.idPret LEFT JOIN batiments on batiments.idLieu = lieu.idLieu WHERE lieu.idLieu = '$idLieu'");
    if ( $reponse) {
	    echo json_encode(array(
	        'success' => true
	    ));
	}
?>