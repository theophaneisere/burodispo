<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $debutDate = '2018-05-01';
    $finDate = '2018-05-31';

    $reponse = $bdd->query("SELECT * FROM prets WHERE '$debutDate' <= prets.dateDebut AND '$finDate' >= prets.dateDebut OR '$debutDate' <= prets.dateFin AND '$finDate' >= prets.dateFin OR '$debutDate' > prets.dateDebut AND '$finDate' < prets.dateFin");

    if ($reponse->rowCount() > 0){
        while ($donnees = $reponse->fetch()) {
            $resultset[] = $donnees;
        }
    } else {
        $resultset[] = null;
    }
    echo json_encode(array(
        'prets' => $resultset,
    ));
?>