<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
	try {
		
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
	}
	catch (Exception $e){
	    die('Erreur : ' . $e->getMessage());
	};
	$json = file_get_contents('php://input'); 	
	$obj = json_decode($json,true);
	$username = $obj['username'];
	$newLieu = $obj['lieu'];
	$newBatiment = $obj['batiment'];
	$newBureau = $obj['bureau'];
	$newEtage = $obj['etage'];
	$newCollegues = $obj['collegues'];
	$newPortable = $obj['portable'];
	$newFixe = $obj['fixe'];
	$newPhone = $obj['phone'];
	$newEthernet = $obj['ethernet'];
	$newReunion = $obj['reunion'];
	$newImprimante = $obj['imprimante'];
	$newCaftiere = $obj['caftiere'];
	$newBouilloire = $obj['bouilloire'];
	$newRefrigerateur = $obj['refrigerateur'];
	$newMicroOnde = $obj['microOnde'];
	$newAscenseur = $obj['ascenseur'];
	$newImage = $obj['image'];
	
	$reponse = $bdd->query("INSERT INTO bureau (usernameProprietaire, bureau, idBatiment, idLieu, etage, portable, fixe, phone, ethernet, reunion, partage, imprimante, caftiere, bouilloire, refrigerateur, microOnde, ascenseur, image) VALUES ('$username', '$newBureau', '$newBatiment', '$newLieu', '$newEtage', '$newPortable', '$newFixe', '$newPhone', '$newEthernet', '$newReunion', '$newCollegues', '$newImprimante', '$newCaftiere', '$newBouilloire', '$newRefrigerateur', '$newMicroOnde', '$newAscenseur', '$newImage')");

	if($reponse){
		echo json_encode(array(
	        'success' => true,
	    ));
	} else {
		echo json_encode(array(
	        'success' => false,
	    ));
	}
?>