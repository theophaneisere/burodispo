<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $idBatiment = $obj['idBatiment'];

    $reponse = $bdd->query("DELETE batiments, bureau, prets, pretsdetails, reservations FROM batiments LEFT JOIN bureau on bureau.idBatiment = batiments.idBatiment LEFT JOIN prets on prets.username = bureau.usernameProprietaire LEFT JOIN pretsdetails on pretsdetails.idPret = prets.idPret LEFT JOIN reservations on reservations.idPret = prets.idPret WHERE batiments.idBatiment = '$idBatiment'");


    if ( $reponse) {
	    echo json_encode(array(
	        'success' => true
	    ));
	}
?>