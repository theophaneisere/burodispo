<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $username = $obj['username'];
    $currentDate = $obj['currentDate'];
    $resultset2 = array();

    $reponse = $bdd->query("SELECT * FROM reservations JOIN prets ON prets.idPret = reservations.idPret JOIN user ON user.username = prets.username JOIN bureau ON bureau.usernameProprietaire = user.username JOIN lieu ON lieu.idLieu = bureau.idLieu WHERE user.username = '$username' and reservations.dateReservation >= '$currentDate' ORDER BY dateReservation, heureDebutReservation"); //Recupere les reservations d'autres utilisateur sur ses prêts'
    

	while ($donnees = $reponse->fetch()) {
        $resultset[] = $donnees;
        $test = $donnees['usernamereservateur'];
        $reponse2 = $bdd->query("SELECT * FROM user JOIN reservations ON reservations.usernamereservateur = user.username WHERE user.username = '$test'");
        if ($reponse2->rowCount() > 0){
            $resultset2[] = $reponse2->fetch();
        }

    }
    if ($reponse->rowCount() == 0){
        $resultset = null;
    }

    echo json_encode(array(
        'infosReservedPrets' => $resultset,
        'infosReservateur' => $resultset2,
        'nbReservedPrets' => $reponse->rowCount(),
    ));
?>