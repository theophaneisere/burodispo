<?php 	
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $currentDate = $obj['currentDate'];
    $currentHour = $obj['currentHour'];
    $startHour = $currentHour.':00:00';
    $endHour = ($currentHour+1).':00:00';

    $reponse = $bdd->query("SELECT * FROM pretsdetails JOIN prets ON pretsdetails.idPret = prets.idPret JOIN bureau ON prets.username = bureau.usernameProprietaire JOIN user ON pretsdetails.usernameReservateur = user.username JOIN batiments ON bureau.idBatiment = batiments.idBatiment WHERE pretsdetails.dateTranche = '$currentDate' AND pretsdetails.heureDebutTranche = '$startHour' AND pretsdetails.heureFinTranche = '$endHour' AND pretsdetails.reserve = '1'"); //Recupere les prêts de l'utilisateur

    if ($reponse){
        if ($reponse->rowCount() > 0){
            while ($donnees = $reponse->fetch()) {
                $resultset[] = $donnees;
            }
            $nbPrets = $reponse->rowCount();
        } else {
            $resultset = null;
             $nbPrets = 0;
        }
    }

    echo json_encode(array(
        'infosPrets' => $resultset,
        'nbPrets' => $nbPrets,
    ));
?>