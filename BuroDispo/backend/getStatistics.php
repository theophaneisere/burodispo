<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
	try {
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
	}
	catch (Exception $e){
	    die('Erreur : ' . $e->getMessage());
	};
	$json = file_get_contents('php://input');
	$obj = json_decode($json,true);
	$debutDate = $obj['debutDate'];
	$finDate = $obj['finDate'];

	if ($debutDate === 'all'){
		$reponse1 = $bdd->query("SELECT idPret, dateDebut, heureDebut, dateFin, heureFin, prenom, nom, bureau, nomLieu FROM prets JOIN user ON prets.username = user.username JOIN bureau ON user.username = bureau.usernameProprietaire JOIN lieu ON bureau.idLieu = lieu.idLieu ORDER BY dateDebut, heureDebut");
		$reponse2 = $bdd->query("SELECT COUNT(*) FROM pretsdetails");
		$reponse3 = $bdd->query("SELECT COUNT(*) FROM reservations");
		$reponse4 = $bdd->query("SELECT COUNT(*) FROM pretsdetails WHERE pretsdetails.reserve = 1");
		$donnees1 = $reponse1->rowCount();
		if ($reponse1->rowCount() > 0){
	        while ($donnees = $reponse1->fetch()) {
	            $idPret = $donnees['idPret'];
	            $reponse5 = $bdd->query("SELECT * FROM pretsdetails WHERE idPret = '$idPret' and reserve = 1");
	            $donnees['nbResa'] = $reponse5->rowCount();
	            $resultset[] = $donnees;
	        }
	    } else {
	        $resultset[] = null;
	    }
		$donnees2 = $reponse2->fetch();
		$donnees3 = $reponse3->fetch();
		$donnees4 = $reponse4->fetch();

		echo json_encode(array(
	        'nbPrets' => $donnees1,
	        'infosPrets' => $resultset,
		    'nbHoursPrets' => $donnees2['0'],
		    'nbReservations' => $donnees3['0'],
		    'nbHoursReservations' => $donnees4['0'],
	    ));
	} else {
		$reponse1 = $bdd->query("SELECT idPret, dateDebut, heureDebut, dateFin, heureFin, prenom, nom, bureau, nomLieu FROM prets JOIN user ON prets.username = user.username JOIN bureau ON user.username = bureau.usernameProprietaire JOIN lieu ON bureau.idLieu = lieu.idLieu WHERE '$debutDate' <= prets.dateDebut and prets.dateDebut <= '$finDate' OR '$debutDate' <= prets.dateFin and prets.dateFin <= '$finDate' or '$debutDate' > prets.dateDebut AND prets.dateFin > '$finDate' ORDER BY dateDebut, heureDebut");
		$reponse2 = $bdd->query("SELECT COUNT(*) FROM pretsdetails WHERE '$debutDate' <= pretsdetails.dateTranche and pretsdetails.dateTranche <= '$finDate'");
		$reponse3 = $bdd->query("SELECT COUNT(*) FROM reservations WHERE '$debutDate' <= reservations.dateReservation and reservations.dateReservation <= '$finDate'");
		$reponse4 = $bdd->query("SELECT COUNT(*) FROM pretsdetails WHERE pretsdetails.reserve = 1 and ('$debutDate' <= pretsdetails.dateTranche and pretsdetails.dateTranche <= '$finDate')");
		$donnees1 = $reponse1->rowCount();
		if ($reponse1->rowCount() > 0){
	        while ($donnees = $reponse1->fetch()) {
	            $idPret = $donnees['idPret'];
	            $reponse5 = $bdd->query("SELECT * FROM pretsdetails WHERE idPret = '$idPret' and reserve = 1");
	            $donnees['nbResa'] = $reponse5->rowCount();
	            $resultset[] = $donnees;
	        }
	    } else {
	        $resultset[] = null;
	    }
		$donnees2 = $reponse2->fetch();
		$donnees3 = $reponse3->fetch();
		$donnees4 = $reponse4->fetch();

		echo json_encode(array(
	        'nbPrets' => $donnees1,
	        'infosPrets' => $resultset,
		    'nbHoursPrets' => $donnees2['0'],
		    'nbReservations' => $donnees3['0'],
		    'nbHoursReservations' => $donnees4['0'],
	    ));
	}
?>