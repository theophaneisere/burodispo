<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
	try {
		
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
	}
	catch (Exception $e){
	    die('Erreur : ' . $e->getMessage());
	};
	$json = file_get_contents('php://input'); 	
	$obj = json_decode($json,true);
	$username = $obj['username'];

	$reponse = $bdd->query("SELECT * FROM bureau WHERE usernameProprietaire = '$username'");
    if ($reponse->rowCount() == 0){
        $reponse2 = $bdd->query("SELECT * FROM user WHERE username = '$username'");
		while ($donnees = $reponse2->fetch()) {
			echo json_encode(array(
				'infos' => $donnees,
				'bureau' => false,
			));
		}
    } else {
        $reponse2 = $bdd->query("SELECT * FROM user 
        	JOIN bureau ON user.username = bureau.usernameProprietaire 
        	JOIN lieu ON bureau.idLieu = lieu.idLieu 
        	JOIN batiments ON bureau.idBatiment = batiments.idBatiment 
        	WHERE user.username = '$username'");
		while ($donnees = $reponse2->fetch()) {
			echo json_encode(array(
				'infos' => $donnees,
				'bureau' => true,
			));
		}
    }
?>