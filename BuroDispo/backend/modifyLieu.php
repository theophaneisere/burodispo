<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
    try {
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
    }
    catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
    };
    $json = file_get_contents('php://input');   
    $obj = json_decode($json,true);
    $nomLieu = $obj['nomLieu'];
    $longitude = $obj['longitude'];
    $latitude = $obj['latitude'];
    $codePostal = $obj['codePostal'];
    $commune = $obj['commune'];
    $idLieu = $obj['idLieu'];

        if ($nomLieu !== ''){
            $reponse = $bdd->query("UPDATE lieu SET lieu.nomLieu = '$nomLieu' WHERE lieu.idLieu = '$idLieu'");
        } else if ($longitude !== ''){
            $reponse = $bdd->query("UPDATE lieu SET lieu.longitude = '$longitude' WHERE lieu.idLieu = '$idLieu'");
        } else if ($latitude !== ''){
            $reponse = $bdd->query("UPDATE lieu SET lieu.lattitude = '$latitude' WHERE lieu.idLieu = '$idLieu'");
        } else if ($codePostal !== ''){
            $reponse = $bdd->query("UPDATE lieu SET lieu.codePostal = '$codePostal' WHERE lieu.idLieu = '$idLieu'");
        } else if ($commune !== ''){
            $reponse = $bdd->query("UPDATE lieu SET lieu.nomCommune = '$commune' WHERE lieu.idLieu = '$idLieu'");
        }
        
        echo json_encode(array(
            'message' => 'Success',
        ));

?>