<?php 
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	include 'config.inc.dev.php';
	try {
		
        $bdd = new PDO('mysql:host='.$HOST_BD.';dbname='.$NAME_BD.';charset=utf8', $LOGIN_BD, $PASSWD_BD);
	}
	catch (Exception $e){
	    die('Erreur : ' . $e->getMessage());
	};
	$json = file_get_contents('php://input'); 	
	$obj = json_decode($json,true);
	$username = $obj['username'];
	$newLieu = $obj['lieu'];
	$newBatiment = $obj['batiment'];
	$newBureau = $obj['bureau'];
	$newEtage = $obj['etage'];
	$newCollegues = $obj['collegues'];
	$newPortable = $obj['portable'];
	$newFixe = $obj['fixe'];
	$newPhone = $obj['phone'];
	$newEthernet = $obj['ethernet'];
	$newReunion = $obj['reunion'];
	$newImprimante = $obj['imprimante'];
	$newCaftiere = $obj['caftiere'];
	$newBouilloire = $obj['bouilloire'];
	$newRefrigerateur = $obj['refrigerateur'];
	$newMicroOnde = $obj['microOnde'];
	$newAscenseur = $obj['ascenseur'];

	if ($newLieu != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET idLieu = '$newLieu' WHERE username = '$username'");
	}
	if ($newBatiment != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET idBatiment = '$newBatiment' WHERE username = '$username'");
	}
	if ($newBureau != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET bureau = '$newBureau' WHERE username = '$username'");
	}
	if ($newEtage != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET etage = '$newEtage' WHERE username = '$username'");
	}
	if ($newCollegues != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET partage = '$newCollegues' WHERE username = '$username'");
	}
	if ($newPortable != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET portable = '$newPortable' WHERE username = '$username'");
	}
	if ($newFixe != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET fixe = '$newFixe' WHERE username = '$username'");
	}
	if ($newPhone != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET phone = '$newPhone' WHERE username = '$username'");
	}
	if ($newEthernet != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET ethernet = '$newEthernet' WHERE username = '$username'");
	}
	if ($newReunion != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET reunion = '$newReunion' WHERE username = '$username'");
	}
	if ($newImprimante != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET imprimante = '$newImprimante' WHERE username = '$username'");
	}
	if ($newCaftiere != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET caftiere = '$newCaftiere' WHERE username = '$username'");
	}
	if ($newBouilloire != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET bouilloire = '$newBouilloire' WHERE username = '$username'");
	}
	if ($newRefrigerateur != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET refrigerateur = '$newRefrigerateur' WHERE username = '$username'");
	}
	if ($newMicroOnde != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET microOnde = '$newMicroOnde' WHERE username = '$username'");
	}
	if ($newAscenseur != ''){
		$reponse = $bdd->query("UPDATE bureau JOIN user ON user.username = bureau.usernameProprietaire SET ascenseur = '$newAscenseur' WHERE username = '$username'");
	}
	$reponseInfos = $bdd->query("SELECT * FROM user JOIN bureau ON user.username = bureau.usernameProprietaire JOIN lieu ON bureau.idLieu = lieu.idLieu JOIN batiments ON bureau.idBatiment = batiments.idBatiment WHERE username = '$username'");
	while ($donnees = $reponseInfos->fetch()) {
		echo json_encode(array(
			'infos' => $donnees,
		));
	}
?>